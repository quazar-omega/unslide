# Unslide

Cipher and decipher ROT-n encrypted text with a simple slider

## [Try it!](https://quazar-omega.codeberg.page/unslide/@pages/dist/?text=kozhiv%2C+dih+mc+rwy+okom%2C+kozihv)


<div align="center">
	<img alt="screenshot" height="500px" src="./screenshot.png">
</div>
