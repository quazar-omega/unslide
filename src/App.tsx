import { createSignal, type Component, Show } from 'solid-js'
import '@fontsource-variable/material-symbols-rounded'

import logo from './logo.svg'
import styles from './App.module.css'

const InfoToast: Component<{ id: string, content: string, modal: boolean }> = (props) => {
	return (
		<dialog id={props.id} class={styles.toast}>
			<div style='display: flex; gap: 10px;'>
				{props.content}
				<Show when={props.modal == true}>
					<form method="dialog">
						<button class={styles.buttonText}>OK</button>
					</form>
				</Show>
			</div>
		</dialog>
	)
}

const App: Component = () => {
	const alphabetLength = 26

	const URLParameters = new URLSearchParams(window.location.search)
	let startingText = URLParameters.get('text') || 'Hello'
	let startingRotation = 0
	if (
		URLParameters.get('rot') !== null &&
		!isNaN(Number(URLParameters.get('rot'))) &&
		Number(URLParameters.get('rot')) <= 26 &&
		Number(URLParameters.get('rot')) >= -26
	) {
		startingRotation = Number(URLParameters.get('rot'))
	}

	const [rotation, setRotation] = createSignal(startingRotation)
	const [text, setText] = createSignal(startingText)
	const [URL, setURL] = createSignal(getCurrentURL())
	const [notice, setNotice] = createSignal({
		'content': '',
		'modal': false
	})
	const [switchCount, setSwitchCount] = createSignal(0)


	function syncRotation(newRotation: number) {
		URLParameters.set('rot', String(newRotation))
		updateURLparameters()

		return newRotation
	}

	function syncText(newText: string) {
		URLParameters.set('text', String(newText))
		updateURLparameters()

		return newText
	}

	function getCurrentURL() {
		return (
			window.location.href.split("?")[0] +
			'?' + URLParameters.toString()
		)
	}

	function updateURLparameters() {
		history.replaceState(null, "", getCurrentURL())
		setURL(getCurrentURL())
	}

	function decipher(text: string, rotation: number): string {
		let decipheredText = Array.from(text).map(letter => {

			if (letter.match(/[a-z]/i)) {
				const characterCode = letter.toLowerCase().charCodeAt(0)
				let calculatedCode = characterCode

				if (characterCode + rotation < 'a'.charCodeAt(0)) {
					calculatedCode = 'z'.charCodeAt(0) + (
						rotation + 1 +
						(calculatedCode - 'a'.charCodeAt(0))
					)
				} else if (characterCode + rotation > 'z'.charCodeAt(0)) {
					calculatedCode = 'a'.charCodeAt(0) + (
						rotation - 1 -
						('z'.charCodeAt(0) - calculatedCode)
					)
				} else {
					calculatedCode = characterCode + rotation
				}

				if (letter == letter.toUpperCase()) {
					return String.fromCharCode(calculatedCode).toUpperCase()
				}
				return String.fromCharCode(calculatedCode)
			}
			return letter

		}).join("")

		return String(decipheredText)
	}

	return (
		<div class={styles.App}>
			<header class={styles.header}>
				<img src={logo} class={styles.logo} alt='logo' />
				<h1>Unslide</h1>
			</header>

			<main class={styles.main}>

				<div class={styles.block}>
					<label class={styles.label} for='cipher'>Cipher text:</label>

					<div class={styles.inputBlock}>

						<textarea
							class={styles.input}
							name='cipher'
							id='cipher'
							placeholder='input some text here...'
							onkeyup={(event) => {
								event.target.style.height = `${event.target.scrollHeight}px`
							}}
							oninput={
								(event) => {
									setText(() => syncText(event.target.value))
								}
							}
							value={text()}
						></textarea>
					</div>
				</div>

				<button title='switch'
					onclick={
						() => {
							setSwitchCount((switchCount() + 1) % 10)
							if (switchCount() == 0) {
								setNotice({ 'content': 'You spin me right \'round, baby, right \'round', 'modal': true })
								document.querySelector('#toast')!.show()
							}

							setText(() => syncText(decipher(text(), rotation())))
							setRotation((rotation) => syncRotation(-rotation))
						}
					} class={styles.materialSymbolsOutlined + ' ' + styles.button} style='align-self: start; justify-self: center'
				>sync</button>

				<div class={styles.block}>
					<label class={styles.label}>Result:</label>
					<div class={styles.inputBlock}>

						<div class={styles.result}>
							<p>{decipher(text(), rotation())}</p>

							<button
								onclick={
									() => {
										let shareOptions = document.querySelector('#share-options')!
										shareOptions.classList.toggle(styles.collapsed)
									}
								}
								class={styles.materialSymbolsOutlined + ' ' + styles.button} style='align-self: end;'
							>share</button>

							<div class={styles.shareOptions + ' ' + styles.collapsed} id='share-options'>
								<button
									onclick={
										async (event) => {
											try {
												await navigator.clipboard.writeText(URL())
												event.target.textContent = 'check'
												event.target.style.color = 'var(--color-affermative)'
												setTimeout(() => {
													event.target.textContent = 'link'
													event.target.style.color = 'black'
												}, 500)
											} catch (error) {
												setNotice({ 'content': 'Failed to copy to clipboard, copy the link from the browser\'s URL bar instead', 'modal': true })
												document.querySelector('#toast')!.show()

												event.target.textContent = 'close'
												event.target.style.color = 'var(--color-negative)'
												setTimeout(() => {
													event.target.textContent = 'link'
													event.target.style.color = 'black'
												}, 500)
											}
										}
									}
									class={styles.materialSymbolsOutlined + ' ' + styles.button}
								>link</button>
							</div>

						</div>

						<div class={styles.slider}>
							<button
								class={styles.materialSymbolsOutlined + ' ' + styles.button}
								onclick={
									() => setRotation(
										(rotation) => {
											return syncRotation(
												rotation - 1 >= -alphabetLength ?
													rotation - 1 :
													rotation
											)
										}
									)
								}
							>remove</button>
							<input
								type='range'
								name='rotation'
								id='rotation'
								min={-alphabetLength}
								max={alphabetLength}
								step={1}
								value={rotation()}
								oninput={
									(event) => {
										setRotation(() => syncRotation(Number(event.target.value)))
									}
								}
							/>
							<button
								class={styles.materialSymbolsOutlined + ' ' + styles.button}
								onclick={
									() => setRotation(
										(rotation) => {
											return syncRotation(
												rotation + 1 <= alphabetLength ?
													rotation + 1 :
													rotation
											)
										}
									)
								}
							>add</button>

							<label class={styles.value} for='rotation'>{rotation()}</label>
						</div>

					</div>
				</div>
			</main>
			<InfoToast id='toast' content={notice().content} modal={notice().modal} />
		</div>
	)
}

export default App
